import React, {Component} from 'react';
import {connect} from 'react-redux';
import {DECREMENT, INCREMENT} from './index';

const increment = (payload) => {
    return {
        type: INCREMENT,
        payload
    }
};

const decrement = (payload) => {
    return {
        type: DECREMENT,
        payload
    }
};

class App extends Component {
    render() {
        return (
            <div className="App">
                wallet: {this.props.wallet}
                <button onClick={()=> this.props.onIncrement(10)}>
                    +10
                </button>
                <button onClick={()=> this.props.onDecrement(10)}>
                    -10
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {wallet: state.wallet}
};

const mapDispatchToProps = (dispatch) => {
    return {
        onIncrement: (payload) => dispatch(increment(payload)),
        onDecrement: (payload) => dispatch(decrement(payload))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
