import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import './styles.scss'


export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

const reducer = (state, action) => {
    switch (action.type) {
        case INCREMENT:
            return Object.assign({}, state, {wallet: state.wallet + action.payload});
        case DECREMENT:
            return Object.assign({}, state, {wallet: state.wallet - action.payload});
        default:
            return state;
    }
};

const initialState = {
    wallet: 0
};

const store = createStore(reducer, initialState);

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));